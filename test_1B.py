from floodsystem.utils import sorted_by_key
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import pytest

def test_distance():
    stations = build_station_list()
    for station in stations:
        if station.name in ['Bourton Dickler']:
            assert station.coord == (51.874767, -1.740083)
    # Show station that is closest to coordinate.
    b = stations_by_distance(stations, (51.874767, -1.740083))
    c = b[0]
    assert c == ('Bourton Dickler', 'Little Rissington', 0.0)
test_distance()

def test_distance1():
    a = MonitoringStation(None, None, 't1', (1, 1), None, None, 'T1')
    b = MonitoringStation(None, None, 't2', (1.1, 1.1), None, None, 'T1')
    c = MonitoringStation(None, None, 't3', (1.3, 1.3), None, None, 'T2')
    d = MonitoringStation(None, None, 't4', (1.2, 1.2), None, None, 'T3')
    stations = [a, b, c, d]
    x = stations_by_distance(stations, (1, 1))
    assert x == [('t1', 'T1', 0.0), ('t2', 'T1', 15.724016124014824), ('t4', 'T3', 31.447768816785352), ('t3', 'T2', 47.17123412565928)]
test_distance1()
