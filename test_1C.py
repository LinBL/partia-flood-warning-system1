from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius
from floodsystem.station import MonitoringStation
import pytest

def test_radius():
    stations = build_station_list()
    for station in stations:
        if station.name in ['Bourton Dickler']:
            assert station.coord == (51.874767, -1.740083)
    # Find stations within 1 m of station above.
    a = stations_within_radius(stations, (51.874767, -1.740083), 0.001)
    assert len(a) == 1
    assert a[0] == 'Bourton Dickler'
test_radius()

def test_radius1():
    a = MonitoringStation(None, None, 't1', (1, 1), None, None, None)
    b = MonitoringStation(None, None, 't2', (1.1, 1.1), None, None, None)
    c = MonitoringStation(None, None, 't3', (1.3, 1.3), None, None, None)
    d = MonitoringStation(None, None, 't4', (1.2, 1.2), None, None, None)
    stations = [a, b, c, d]
    x = stations_within_radius(stations, (1, 1), 35)
    assert x == ['t1', 't2', 't4']
test_radius1()


