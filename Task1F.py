from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list();

    q = (inconsistent_typical_range_stations(stations))
    q.sort()
    print(q)

if __name__ == "__main__":
    run()
