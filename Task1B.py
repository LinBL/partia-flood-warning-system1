from floodsystem.utils import sorted_by_key
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from math import radians, cos, sin, asin, sqrt

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Find the distance of the stations from a point in central Cambridge
    dist = stations_by_distance(stations, (52.2053, 0.1218))

    # Print the nearest 10 stations
    print("Nearest 10 stations = ", dist[:10] )

    # Print the furthest 10 stations
    print("Furthest 10 stations = ", dist[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IB Flood Warning System ***")

    # Run Task1B
    run()
