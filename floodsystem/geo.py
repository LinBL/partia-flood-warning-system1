"""This module contains a collection of functions related to
geographical data.

"""
import numpy as np

from .utils import sorted_by_key

from math import radians, cos, sin, asin, sqrt

AVG_EARTH_RADIUS = 6371  # in km

def haversine(point1, point2, miles=False):
    """ Calculate the great-circle distance between two points on the Earth surface.
    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.
    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))
    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.
    """
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles:
        return h * 0.621371  # in miles
    else:
        return h  # in kilometers

def stations_by_distance(stations, p):
    """Find the distance of a station from a particular point 'p'."""
    distance = []
    for station in stations:
         a = (station.name, station.town, haversine(p, station.coord))
         distance.append(a)
    return sorted_by_key(distance, 2)

def stations_within_radius(stations, centre, r):
    """Find and list the stations within a certain radius of a particular central point."""
    radius = []
    for station in stations:
        a = haversine(centre, station.coord)
        if a <= r:
            radius.append(station.name)
    return sorted(radius)

def rivers_with_station(stations):
        """This function expects a list of MonitoringStation class objects and returns all rivers
        with monitoring station"""
        x = set();
        for n in range(len(stations)):
            x.add(stations[n].river)
        y = sorted(x);
        return y;
        
def stations_by_river(stations):
    """This function expects a list of MonitoringStation class objects and returns
    a dictionary which maps rivers to a list of stations on said river"""
    x = {}
    for i in range (len(stations)):
        if x.get(stations[i].river) == None:
            x[stations[i].river] = []
            x[stations[i].river].append(stations[i])
        else:
            x[stations[i].river].append(stations[i])
    return x;

def rivers_by_station_number(stations, N):
    """This function expects a list of MonitoringStation class and an integrer N. It will return a list of
     N touples of rivers with the biggest number of stations and the number of stations.
      Please note that it will return list of more than N elements, if there are more rivers with the lowest number of
      stations in the list"""
    z = stations_by_river(stations)
    x = []
    for key in z:
        temp = [key, len(z[key])]
        x.append(temp)
    x.sort(key = lambda x: x[1])
    x.reverse()
    temp = [0,500000]
    x.append(temp)
    temp = [0,600000]
    #Safety measures
    x.append(temp)
    x.append(temp)
    x.append(temp)
    z = []
    q = 0
    z.append(tuple(x[0]))
    while q < N or x[q][1]==x[q+1][1]:
        q += 1
        if x[q][1] < 40000:
            z.append(tuple(x[q]));
    return z;