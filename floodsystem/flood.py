from .station import MonitoringStation
from . import datafetcher
from floodsystem.utils import sorted_by_key
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from floodsystem import geo



def stations_level_over_threshold(stations, tol):
    """Find the stations where water levels are above tolerance."""
    over = []
    for station in stations:
        rel = station.relative_water_level()
        a = (station, rel)
        if rel > tol:
            over.append(a)
    return sorted_by_key(over, 1, True)

def stations_highest_rel_level(stations, N):
    """Find the N stations where the relative water levels are the highest."""
    for station in stations:
        b = stations_level_over_threshold(stations, 0)
        return b[:N]