import datetime
from floodsystem.station import MonitoringStation
import numpy as np
import matplotlib.pyplot as plt
from plot import plot_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels
from floodsystem.datafetcher import fetch_measure_levels

dt = 10
stations = build_station_list()
update_water_levels(stations)
q = stations_highest_rel_level(stations, 5)
for i in range(len(q)):
    dates, levels = fetch_measure_levels(q[i][0].measure_id,
                                         dt=datetime.timedelta(days=dt))
    plot_water_levels(q[i][0], dates, levels)

plt.show()