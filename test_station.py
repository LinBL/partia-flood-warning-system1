"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    assert s.typical_range_consistent() == True

    s.typical_range = (100, -2)

    assert s.typical_range_consistent() == False

    s.typical_range = None
    assert s.typical_range_consistent() == False
    
    s = MonitoringStation(None,None,'s', None,  None, None, None)
    q = MonitoringStation(None, None, 'q',None, (0.3, -0.1), None, None)
    y = MonitoringStation(None, None,'y',None, (30, 40), None, None)
    z = MonitoringStation(None, None,'z', None, (10, 20), None, None)
    t = [s, y, z, q]
    assert inconsistent_typical_range_stations(t) == ['s', 'q']

test_create_monitoring_station()