from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.analysis import polyfit
import numpy as np
from floodsystem.geo import stations_by_river
stations = build_station_list();
from floodsystem.station import MonitoringStation
import matplotlib
from floodsystem.datafetcher import fetch_measure_levels
import datetime

stations = build_station_list()
dt = 2
moderate_cof = 1

const_severe= 2
const_high = 0.1
severe = []
high = []
moderate = []
low = stations
update_water_levels(stations)
q = stations_level_over_threshold(stations, moderate_cof)
for i in range (len(q)):
    x, y = fetch_measure_levels(q[i][0].measure_id, dt=datetime.timedelta(days=dt))
    if x == [] or y == []:
        print('Nope')
    else:
        p , offset = polyfit(x, y, 4)
        p = np.polyder(p, 1)
        #print (p(matplotlib.dates.date2num(x[-1]) - offset) )
        if p(matplotlib.dates.date2num(x[-1]) - offset) > const_severe:
            severe.append(q[i][0])
            low.remove(q[i][0])
        elif p (matplotlib.dates.date2num(x[-1]) - offset) > const_high and p (matplotlib.dates.date2num(x[-1]) - offset) < const_severe:
            high.append(q[i][0])
            low.remove(q[i][0])
        else:
            moderate.append(q[i][0])
            low.remove(q[i][0])
for i in range (len(severe)):
    if severe[i].town != None:
        print(severe[i].town)



