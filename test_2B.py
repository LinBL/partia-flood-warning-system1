from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import pytest
from floodsystem.flood import stations_level_over_threshold


def test_relwaterlev():
    a = MonitoringStation(None, None, 't1', None, (0.1, 0.5), None, None)
    b = MonitoringStation(None, None, 't2', None, (1.5, 1.6), None, None)
    c = MonitoringStation(None, None, 't3', None, (0.2, 0.1), None, None)
    d = MonitoringStation(None, None, 't4', None, (1.1, 1.3), None, None)
    stations = [a, b, c, d]
    for station in stations:
        station.latest_level = 1.2
    x = stations_level_over_threshold(stations, 0.3)
    assert len(x) == 2
    assert x[0][0].name == 't1'
    assert x[0][1] == 2.7499999999999996
    assert x[1][0].name == 't4'
    assert x[1][1] == 0.49999999999999944
test_relwaterlev()