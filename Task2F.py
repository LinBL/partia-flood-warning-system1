import numpy as np
from plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list
import datetime
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels

stations = build_station_list()

dt = 2

update_water_levels(stations)
tempor = stations_highest_rel_level(stations, 5)
for i in range(len(tempor)):
    if tempor[i][0].name ==  'Bampton Grange':
        print('Nope')
    else:
        x, y = fetch_measure_levels(tempor[i][0].measure_id, dt=datetime.timedelta(days = dt))
        plot_water_level_with_fit(tempor[i][0], x, y, 4)

plt.show()
