
from  floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list
import numpy as np
import matplotlib.pyplot as plt
import datetime as dat
from floodsystem.analysis import polyfit
import matplotlib

def plot_water_levels(station, dates, levels):
    """This function takes the name of the station. The touple of boundary dates and touple of critical levels and returns a plot
     of water level agains time for this station along with marked critical level lines. It supports multiple figures
     You can call this function for as any plots as you want, just type plt.show() afterwards"""
    plt.figure()
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    plt.axhline(y = min(station.typical_range), xmin = 0, xmax = 1, hold = None)
    plt.axhline(y = max(station.typical_range), xmin = 0, xmax = 1, hold = None)
    plt.tight_layout()

def plot_water_level_with_fit(station, dates, levels, p):
    poli , d0 = polyfit(dates,levels,p);
    x = dates
    y1 = levels
    y2 = poli(matplotlib.dates.date2num(x) - d0)
    plt.figure()
    plt.plot(x,y1, '-g')
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name + ' (true values) ' )
    plt.axhline(y=min(station.typical_range), xmin=0, xmax=1, hold=None)
    plt.axhline(y=max(station.typical_range), xmin=0, xmax=1, hold=None)
    plt.plot(x, y2)
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name + ' (polyfit) ')
    plt.axhline(y=min(station.typical_range), xmin=0, xmax=1, hold=None)
    plt.axhline(y=max(station.typical_range), xmin=0, xmax=1, hold=None)
    plt.legend(['true values', 'polyfit'])
