import numpy as np
from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def test_D():
    y = MonitoringStation(None, None, 't1', None, None, '1', None)
    x = MonitoringStation(None, None, 't2', None, None, '2', None)
    z = MonitoringStation(None, None, 't3', None, None, '2', None)
    p = MonitoringStation(None, None, 't4', None, None, '3', None)
    stations = [y, x, z, p]
    assert (rivers_with_station(stations)) == ['1', '2' ,'3']
    assert stations_by_river(stations) ==  {'1': ['t1'], '2': ['t2', 't3'], '3': ['t4']}