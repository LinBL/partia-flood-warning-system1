from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
        """Requirements for Task 1C"""

        # Build list of stations
        stations = build_station_list()

        # Find the stations within 10km of the point in the centre of Cambridge
        rad = stations_within_radius(stations, (52.2053, 0.1218), 10)

        # Print the stations
        print("Stations with 10km: ", rad)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part 1C Flood Warning System ***")

    # Run Task1C
    run()