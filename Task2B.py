from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold


def run():
    """Requirements for Task 2B"""

    # Build list of stations
    stations = build_station_list()

    # Update the water levels of all the stations
    update_water_levels(stations)

    # Create a list of stations above the threshold level
    a = stations_level_over_threshold(stations, 0.8)
    for i in range(len(a)):
        print(a[i][0].name, ' ', a[i][1])


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

    run()