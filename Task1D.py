import numpy as np
from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    z = rivers_with_station(stations)
    print(len(z))
    x = []
    for i in range (10):
        x.append(z[i])
    print(x, '\n');

    z = stations_by_river(stations)
    x = sorted (z['River Aire'])
    y = sorted (z['River Cam'])
    q = sorted (z['Thames'])

    print(x, '\n')
    print(y, '\n')
    print('\n')
    print(q, '\n')

if __name__ == "__main__":
    run()