import numpy as np
import pytest
from floodsystem.geo import stations_by_river, rivers_by_station_number
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
def test_E():
    y = MonitoringStation(None, None, 't1', None, None, '1', None)
    x = MonitoringStation(None, None, 't2', None, None, '2', None)
    z = MonitoringStation(None, None, 't3', None, None, '2', None)
    p = MonitoringStation(None, None, 't4', None, None, '3', None)
    v = MonitoringStation(None, None, 't5', None, None, '4', None)
    u = MonitoringStation(None, None, 't5', None, None, '3', None)
    stations = [y, x , z, p]
    assert rivers_by_station_number(stations,2) == [('2', 2), ('3',1), ('1',1)]
    assert rivers_by_station_number(stations,3) == [('2', 2), ('3',1), ('1',1)]