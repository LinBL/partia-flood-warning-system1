from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level


def run():
    """Requirements for Task 2C"""

    # Build list of stations
    stations = build_station_list()

    # Update the water levels of all the stations
    update_water_levels(stations)

    # Find the stations with the highest relative water levels
    highwater = stations_highest_rel_level(stations, 10)

    # Print the stations
    print("Stations with highest relative levels: ")
    for i in range(len(highwater)):
        print(highwater[i][0].name, ' ', highwater[i][1])


if __name__ == "__main__":
    print("*** Task 2C: CUED Part 1A Flood Warning System ***")

    # Run Task2C
    run()